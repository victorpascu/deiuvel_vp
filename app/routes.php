<?php

Route::get('/', function()
{
	return View::make('home');
});

Route::get('/contact', function()
{
	return Response::json
	([
		'company' => 'Deiusoft',
		'email' => 'hello@deiusoft.com'
	]);
});

Route::get('/contact-us', function()
{
	return Redirect::to('/contact');
});

// 1. Input::get(), Config::get() ... Ioana + Dan
// 2. Cache::get(), Cache::set() ... Mirela
// 3. Route::post(), Route::any() ... Erika + Norbert
// 4. View::include(), View::composer(), layout? ... Victor + Tudor