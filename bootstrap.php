<?php

$paths = array
(
	'app' => __DIR__.'/app',
	'public' => __DIR__.'/public',
	'framework' => __DIR__.'/vendor/deiuvel',
);

// require all framework files

foreach (glob($paths['framework'].'/*.php') as $filename)
{
	require_once $filename;
}

require_once 'app/composers.php';
require_once 'app/start.php';
require_once 'app/routes.php';

App::run();