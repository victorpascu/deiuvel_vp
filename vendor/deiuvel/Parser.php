<?php

class Parser
{
	static $finalHtml;

	public static function layoutParse($filename)
	{

		$path = get_path('app').'/views/'.$filename.'.deiurazor.php';
		$fileContent = file_get_contents($path);
		
		if ($extendsPos = strpos($fileContent, '@extends') == 1)
		{
			$extendName = substr($fileContent, $extendsPos+10, strpos($fileContent, '\'', $extendsPos+10)-$extendsPos-10);

			static::layoutParse($extendName);
		}
		else static::$finalHtml=$fileContent;

		$tempHtml = static::$finalHtml;
		$yieldPos = 0;

		while ($yieldPos = strpos($tempHtml, '@yield', $yieldPos+1))
		{
			$sectionName = substr($tempHtml, $yieldPos+8, strpos($tempHtml, '\'', $yieldPos+8)-$yieldPos-8);


			if ($sectionPos = strpos($fileContent, '@section \''.$sectionName.'\''))
			{
				$stopPos = strpos($fileContent, '@stop', $sectionPos+1);

				$nameLength = $sectionPos+8+strlen($sectionName)+3;

				$tempHtml = str_replace('@yield \''.$sectionName.'\'', substr($fileContent, $nameLength, $stopPos-$nameLength), $tempHtml);
			}
		}

		static::$finalHtml = $tempHtml;
	}

	public static function accoladeParse()
	{
		$tempHtml = static::$finalHtml;

		while ($accoladePos = strpos($tempHtml, '{{', $accoladePos+1))
		{
			$sectionName = substr($tempHtml, $accoladePos+2, strpos($tempHtml, '}}', $accoladePos+2)-$accoladePos-2);

			$tempHtml = str_replace('{{'.$sectionName.'}}', '<?php echo '.$sectionName.'; ?>', $tempHtml);
		}

		static::$finalHtml = $tempHtml;
	}

	public static function startParse($filename)
	{
		static::layoutParse($filename);
		static::accoladeParse();

		$folder = str_replace("//", "/", $_SERVER['DOCUMENT_ROOT'].'/deiuvel'."/app/storage/views/");

		file_put_contents($folder.$filename.'.php', static::$finalHtml);
	}
}