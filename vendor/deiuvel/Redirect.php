<?php

class Redirect
{
	public static function to($uri)
	{
		header('Location: '.get_prefix_path().$uri);
		die();
	}
}