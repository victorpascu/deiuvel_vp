<?php

class View
{
	protected static $composerData = [];

	public static function make($filename, $data = [])
	{
		$path = get_path('app').'/views/'.$filename.'.deiurazor.php';

		if (file_exists($path))
		{
			Parser::startParse($filename);
			$path = get_path('app').'/storage/views/'.$filename.'.php';
		}
		else 
			$path = get_path('app').'/views/'.$filename.'.php';

		if (array_key_exists($filename, static::$composerData))
			$data = array_merge(static::$composerData[$filename], $data);

		ob_start();

		extract($data);
		unset($data);

		require $path;
		
		return ob_get_clean();
	}

	public static function viewInclude($filename)
	{
		$path = get_path('app').'/views/'.$filename.'.php';

		if (!file_exists($path)) return "Not found";

		require $path;
	}

	public static function composer($filename, $data = [])
	{
		static::$composerData[$filename] = $data;
	}
}